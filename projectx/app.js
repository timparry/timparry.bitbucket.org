
var cartCountId = document.getElementById('js-betslip-cart--count');
var cardCount = 0;

requirejs(['./dist/projectx'], function createProjectX(ProjectX) {
    ProjectX.default({
        domRoot: document.getElementById('projectx'),
        betslip: {
            addOutcome: function() {
                cardCount++;
                cartCountId.innerText = cardCount;
            }
        },
        reactData: {
            'cards':[{
                'class':'Football',
                'event': 'Liverpool vs Arsenal',
                'market': 'Match Result',
                'outcome': 'Liverpool to Win',
                'price': '5/1',
                'outcomeId': 22222
            },
            {
                'class':'Tennis',
                'event': 'Murray vs Nadal',
                'market': 'Match Result',
                'outcome': 'Murray to Win',
                'price': '15/1',
                'outcomeId': 3333
            },
            {
                'class':'Bowls',
                'event': 'Izzy vs Timmy',
                'market': 'Match Result',
                'outcome': 'Izzy to Win',
                'price': '1/5',
                'outcomeId': 4444
            },
            {
                'class':'Darts',
                'event': 'Betsy vs Mummy',
                'market': 'Match Result',
                'outcome': 'Betsy to Win',
                'price': '1/50',
                'outcomeId': 444454
            }]

        }
    })
});