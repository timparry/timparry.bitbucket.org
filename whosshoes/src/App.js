import React, { useState } from "react";
import { css, StyleSheet } from "aphrodite";

export default () => {

  const [shoe, setShoe] = useState(0);
  const [show, setShow] = useState(false);
  const [reveal, setReveal] = useState(false);

  const persons = [

    {
      name: "Laylaski",
      img: "layla.png"
    },
    {
      name: "Ashleyski",
      img:"ashley.jpg"
    },
    {
      name: "Adamski",
      img:"adam.jpg"
    },
    {
      name: "Andyski",
      img:"andy.jpg"
    },
    {
      name: "Roryski",
      img:"rory.jpg"
    },
    {
      name: "Chrisski",
      img:"chris.jpg"
    },
    {
      name: "Sharynski",
      img:"sharyn.jpg"
    },
    {
      name: "Cyski",
      img:"cy.jpg"
    },
    {
      name: "Timski",
      img:"tim.jpg"
    },
    {
      name: "Jesski",
      img:"jess.jpg"
    },
    {
      name: "Lisaski",
      img:"lisa.jpg"
    },
    {
      name: "Jacobski",
      img:"jacob.jpg"
    },
    {
      name: "Alexski",
      img: "alex.jpg"
    }
    ]

  const { name, img } = persons[shoe];
  const showing = `https://timparry.bitbucket.io/images/shoes/${img}`;

  return (
    <div className={css(styles.main)}>
    {!show && <div></div>}
    <div><h1>Who's Shoes Are Those?</h1>
      <p>The Yello-Jello Shoe Guessing Game</p>
      <p>With 4 Featured Guests (and 1 who's not been able to join us)</p>
    </div>
    {!show && <div className={styles.buttonHolder}><button className={css(styles.button)} onClick={()=>setShow(true)}>START GUESSING</button></div>}

    {show && <>
      <div className={css(styles.pictureHolder)}>
        <div className={css(styles.picture)}>
          <img className={css(styles.shoePic)} src={showing}/>
          {reveal && <div className={css(styles.person)}>{name}</div>}
        </div>
      </div>
      <div className={css(styles.buttonHolder)}>
        <button className={css(styles.button)} disabled={shoe === 0  ? "disabled" : false} onClick={()=>{
          setShoe(shoe-1);
          setReveal(false);
        }}>Previous</button>
        <button className={css(styles.button)} disabled={shoe < persons.length -1 ? false : "disabled"} onClick={()=>{
          setShoe(shoe+1)
          setReveal(false)
        }}>Next</button>
        <button className={css(styles.button)} onClick={()=>setReveal(true)}>Reveal</button>
      </div>
    </>
    }
  </div>
)
};

const styles = StyleSheet.create({
  main: {
    fontFamily: "Garamond",
    display: "flex",
    flexDirection: "column",
    height: "90vh",
    justifyContent: "space-between",
    textAlign: "center"
  },
  buttonHolder: {
    margin: "auto",
    marginTop: "21px",
    display: "flex",
    justifyContent: "space-between",
    maxWidth: "400px",
    textAlign: "center"
  },
  pictureHolder: {
    height: "800px",
    width: "800px",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    margin: "auto",
    backgroundImage: "url('https://timparry.bitbucket.io/images/frames/frame0.png')",
    backgroundSize: "contain",
    backgroundRepeat:"no-repeat",
    backgroundPosition: "center"
  },
  picture: {
    height: "70%",
    width: "40%",
    border: "10px solid black",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    position: "relative"
  },
  shoePic: {
    maxWidth: "320px",
    maxHeight: "400px"
  },
  person: {
    position: "absolute",
    bottom: "8px",
    backgroundColor: "#ffdf00",
    padding: "8px",
    fontFamily: "Garamond"
  },
  button: {
    backgroundColor: "#ffdf00",
    fontSize: "21px",
    margin: "0 14px",
    borderRadius: "8px"
  }
});
